package rs.devcenter.battlearena;

import rs.devcenter.battlearena.hero.*;

import java.io.File;
import java.sql.*;

/**
 * This class is used to manipulate DataBase.
 *
 * @author Nemanja Rajkovic
 * @version 1.0
 * @since 24.10.2017
 */
public class DBConnection {

    private static Connection connection;

    /**
     * This method makes connection with DataBase.
     *
     * @return connection to Data Base.
     */
    public static Connection connect() throws
            SQLException {
        if (connection == null) {
            String url = "jdbc:sqlite:BattleHero.db";
            connection = DriverManager.getConnection(url);
            return connection;
        } else
            return connection;
    }

    /**
     * This method checks if Data Base exists.
     */
    public void checkIfDBExists() {
        String path = "BattleHero.db";
        File f = new File(path);
        if (!f.exists()) {
            try {
                createNewDataBase();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else return;
    }

    /**
     * This method will create new Data Base and call createNewTable method.
     */
    public void createNewDataBase() throws SQLException {

        String url = "jdbc:sqlite:BattleHero.db";
        try (Connection conn = DriverManager.getConnection(url)) {
            if (conn != null) {
                DatabaseMetaData meta = conn.getMetaData();
                System.out.println("The driver name is " + meta.getDriverName());
                System.out.println("A new database has been created.");
                createNewTable();
                insertIntoDB("Clartred", "priest", 1, 1);
                insertIntoDB("Ekonomicar", "mage", 1, 1);
                insertIntoDB("Thrall", "shaman", 1, 1);
                insertIntoDB("Gul'Dan", "warlock", 1, 1);
                insertIntoDB("Weritas", "hunter", 1, 1);
            }
        }
    }

    /**
     * This methods creates new table if one under that name doesn't already exists.
     */
    public void createNewTable() throws SQLException {
        Connection conn = this.connect();
        System.out.println("Table is created.");
        String createTable = "Create table if not exists hero (name varchar(20) , class varchar(20),currLV int default 0,  currXP float)";
        Statement statement = conn.createStatement();
        statement.execute(createTable);
    }

    /**
     * @param name   of the Hero we want to put in DataBase.
     * @param Class  of the Hero we want to put in DataBase.
     * @param currLv of the Hero we want to put in DataBase.
     * @param currXP of the Hero we want to put in DataBase.
     */
    public void insertIntoDB(String name, String Class, int currLv, double currXP) throws SQLException {
        Connection conn = this.connect();
        String sql = "INSERT INTO hero(name, Class ,currLv, currXP) VALUES(?,?,?,?)";
        PreparedStatement preparedStatement = conn.prepareStatement(sql);
        preparedStatement.setString(1, name);
        preparedStatement.setString(2, Class);
        preparedStatement.setInt(3, currLv);
        preparedStatement.setDouble(4, currXP);
        preparedStatement.executeUpdate();
    }

    /**
     * This method will save Hero's current state every time he is done with a battle.
     *
     * @param h1        is hero that will be saved.
     * @param className is his class name and parametar for searching in Database.
     */
    public void updateTable(Hero h1, String className) throws SQLException {
        Connection conn = this.connect();
        // String update = "UPDATE hero set currLV = ? ," + "currXP = ? " + " where class = "+className;
        String update = "UPDATE hero set currLV = ? ," + "currXP = ? " + "  WHERE class = '" + className + "'";
        PreparedStatement preparedStatement = conn.prepareStatement(update);
        preparedStatement.setInt(1, h1.getCurrentLv());
        preparedStatement.setDouble(2, h1.getCurrentHP());
        preparedStatement.executeUpdate();
    }

    /**
     * This method will check DataBase for the Hero with @name, if it exists, it will retrieve it.
     *
     * @return Hero from DataBase to user.
     */
    public String[] retrieveHero(String name) throws SQLException {
        String[] heroStats = new String[3];
        Connection conn = this.connect();
        String selectAll = "SELECT * FROM hero where class = '" + name + "'";
        Statement statement = conn.createStatement();
        ResultSet resultSet = statement.executeQuery(selectAll);
        heroStats[0] = resultSet.getString("name");
        heroStats[1] = resultSet.getInt("currLv") + "";
        heroStats[2] = resultSet.getDouble("currXP") + "";
        return heroStats;
    }
}