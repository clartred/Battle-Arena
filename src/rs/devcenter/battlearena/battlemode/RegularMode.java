package rs.devcenter.battlearena.battlemode;

import rs.devcenter.battlearena.DBConnection;
import rs.devcenter.battlearena.combat.UserVsAICombat;
import rs.devcenter.battlearena.hero.Hero;
import rs.devcenter.battlearena.logger.HighScore;

import java.io.IOException;
import java.sql.SQLException;

/**
 * This class will allow fight between User and AI.
 *
 * @author Nemanja Rajkovic
 * @version 1.0
 * @since 21.10.2017
 */
public class RegularMode extends BattleMode {

    public RegularMode(Hero h1, Hero h2) {
        this.setHero1(h1);
        this.setHero2(h2);
    }

    /**
     * This method is using while loop to continuously run
     * userCombatSystem() and AICombatSystem() methods until
     * only one of the Heroes is alive.
     */
    @Override
    public void startCombat() {
        UserVsAICombat userVsAICombat = new UserVsAICombat(getHero1(), getHero2());
        //will go on until one of heroes is alive.
        System.out.println(getHero1().getName() + " has " + getHero1().getCurrentHP() + " health and "+getHero1().getCurrentMana() + " mana");
        System.out.println(getHero2().getName() + " has " + getHero2().getCurrentHP() + " health and "+getHero2().getCurrentMana() + " mana\n");

        while (getHero1().getCurrentHP() > 0 & getHero2().getCurrentHP() > 0) {
            //AI's turn.
            userVsAICombat.AICombat();
            if (getHero1().getCurrentHP() <= 0) {
                break;
            }
            //User's turn.
            userVsAICombat.userCombat();
            if (getHero2().getCurrentHP() <= 0) {
                //giving XP to the User if he won
                getHero1().setCurrentLv(getHero1().getCurrentLv() + 1);
                break;
            }
        }
        String winner = (getHero1().getCurrentHP() > getHero2().getCurrentHP()) ? getHero1().getName() : getHero2().getName();
        System.out.println("Winner is: " + winner);
        System.out.println("Number of rounds was: " + userVsAICombat.getRoundCounter());
        System.out.println("Uulwi ifis halahs gag erh'ongg w'ssh!");
        DBConnection dbConnection = new DBConnection();
        //Saving User's score in Database.
        try {
            dbConnection.updateTable(getHero1(), getHero1().getHeroClass());
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        HighScore highScore = new HighScore();
        try {
            highScore.gettingHighScore();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}