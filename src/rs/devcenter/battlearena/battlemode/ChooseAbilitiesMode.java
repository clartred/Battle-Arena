package rs.devcenter.battlearena.battlemode;

import rs.devcenter.battlearena.DBConnection;
import rs.devcenter.battlearena.effectovertime.*;
import rs.devcenter.battlearena.hero.Hero;
import rs.devcenter.battlearena.spells.*;

import java.sql.SQLException;
import java.util.Random;
import java.util.Scanner;

/**
 * This class is Battle Mode which lets User to choose abilities.
 *
 * @author Nemanja Rajkovic.
 * @version 2.0
 * @date 4.11.2017
 */
public class ChooseAbilitiesMode extends BattleMode{

    private Scanner scanner = new Scanner(System.in);

    public ChooseAbilitiesMode(Hero h1, Hero h2) {
        this.setHero1(h1);
        this.setHero2(h2);
    }

    /**
     * This method is starting Game. It forwards User to choose Hero, Arena, Spells and Dots/Hots.
     */
    public void startCombat() {
        this.chooseSpells(getHero1());
        this.chooseSpells(getHero2());
        this.chooseDotsHots(getHero1());
        this.chooseDotsHots(getHero2());
        RegularMode regularMode = new RegularMode(getHero1(), getHero2());
        regularMode.startCombat();
    }

    /**
     * This method lets User choose which spells he wants to use.
     *
     * @param hero, is the Hero that will recieve spells.
     */
    public void chooseSpells(Hero hero) {
        System.out.print("Press : 1 for Arcane Blast, 2 for Arcane Shot, 3 for Curse of Man'ari, 4 for Drain Life,");
        System.out.println("5 for Elemental Strike, 6 for Fireball, 7 for Holy Light, 8 for Image of Medvieh, ");
        System.out.print("9 Lava Burst, 10 for Might of Rexar, 11 for Might of Yogg'Saron, 12 for Rage of Argus, ");
        System.out.println("13 for Raptor Strike, 14 for Touch of Nature, 15 for Whispers of N'zoth");
        Scanner scanner = new Scanner(System.in);
        for (int j = 0; j < 3; j++) {
            System.out.println("Pick Spell Number " + j + 1);
            int i = scanner.nextInt();
            if (i == 1) {
                ArcaneBlast arcaneBlast = new ArcaneBlast(hero);
                hero.setHeroSpells(arcaneBlast, j);
            } else if (i == 2) {
                ArcaneShot arcaneShot = new ArcaneShot(hero);
                hero.setHeroSpells(arcaneShot, j);
            } else if (i == 3) {
                CurseOfManari curseOfManari = new CurseOfManari(hero);
                hero.setHeroSpells(curseOfManari, j);
            } else if (i == 4) {
                DrainLife drainLife = new DrainLife(hero);
                hero.setHeroSpells(drainLife, j);
            } else if (i == 5) {
                ElementalStrike elementalStrike = new ElementalStrike(hero);
                hero.setHeroSpells(elementalStrike, j);
            } else if (i == 6) {
                FireBall fireBall = new FireBall(hero);
                hero.setHeroSpells(fireBall, j);
            } else if (i == 7) {
                HolyLight holyLight = new HolyLight(hero);
                hero.setHeroSpells(holyLight, j);
            } else if (i == 8) {
                ImageOfMedvieh imageOfMedvieh = new ImageOfMedvieh(hero);
                hero.setHeroSpells(imageOfMedvieh, j);
            } else if (i == 9) {
                LavaBurst lavaBurst = new LavaBurst(hero);
                hero.setHeroSpells(lavaBurst, j);
            } else if (i == 10) {
                MightOfRexar mightOfRexar = new MightOfRexar(hero);
                hero.setHeroSpells(mightOfRexar, j);
            } else if (i == 11) {
                MightOfYoggSaron mightOfYoggSaron = new MightOfYoggSaron(hero);
                hero.setHeroSpells(mightOfYoggSaron, j);
            } else if (i == 12) {
                RageOfArgus rageOfArgus = new RageOfArgus(hero);
                hero.setHeroSpells(rageOfArgus, j);
            } else if (i == 13) {
                RaptorStrike raptorStrike = new RaptorStrike(hero);
                hero.setHeroSpells(raptorStrike, j);
            } else if (i == 14) {
                TouchOfNature touchOfNature = new TouchOfNature(hero);
                hero.setHeroSpells(touchOfNature, j);
            } else if (i == 15) {
                WhispersOfNzoth whispersOfNzoth = new WhispersOfNzoth(hero);
                hero.setHeroSpells(whispersOfNzoth, j);
            }
        }
    }

    /**
     * This method lets User to choose which Dot/Hot he wants to use.
     *
     * @param hero, is the Hero that will recieve Dots/Hots.
     */
    public void chooseDotsHots(Hero hero) {
        System.out.println("Choose Dot/Hot. Press : 1 for Blessing of Na'aru, 2 for Doom, 3 for Fire Bomb, 4 for Healing Totem, 5 for Snake Bite");
        int i = scanner.nextInt();
        switch (i) {
            case 1: {
                BlessingOfNaaru blessingOfNaaru = new BlessingOfNaaru(hero);
                hero.setEffectsOverTime(blessingOfNaaru);
            }
            break;
            case 2: {
                Doom doom = new Doom(hero);
                hero.setEffectsOverTime(doom);
            }
            break;
            case 3: {
                FireBomb fireBomb = new FireBomb(hero);
                hero.setEffectsOverTime(fireBomb);
            }
            break;
            case 4: {
                HealingTotem healingTotem = new HealingTotem(hero);
                hero.setEffectsOverTime(healingTotem);
            }
            break;
            case 5: {
                SnakeBite snakeBite = new SnakeBite(hero);
                hero.setEffectsOverTime(snakeBite);
            }
            break;
        }
    }
}