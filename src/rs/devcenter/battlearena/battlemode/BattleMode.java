package rs.devcenter.battlearena.battlemode;

import rs.devcenter.battlearena.hero.*;

import java.sql.SQLException;

/**
 * This abstract class is a blueprint for other Battle-Modes.
 *
 * @author Nemanja Rajkovic
 * @version 1.0
 */
public abstract class BattleMode {
    private String name;
    private Hero hero1, hero2;

    public abstract void startCombat() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException;

    public String getName() {
        return name;
    }

    public void setName(String name) {

        this.name = name;
    }

    public Hero getHero1() {
        return hero1;
    }

    public void setHero1(Hero hero1) {
        this.hero1 = hero1;
    }

    public Hero getHero2() {
        return hero2;
    }

    public void setHero2(Hero hero2) {
        this.hero2 = hero2;
    }
}
