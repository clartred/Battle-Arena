package rs.devcenter.battlearena.logger;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.output.TeeOutputStream;

import java.io.*;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * This class will log whole output to the text file.
 * Every day, new text file will be created.
 *
 * @author Nemanja Rajkovic
 * @version 1.0
 * @date 6.11.2017
 */
public class BattleLogger {

    FileOutputStream fileOutputStream;

    /**
     * logging is a method which creates a file named by the correct date(dd,mm,yyyy)
     * and loggs all output from console into a text file.
     */
    public void logging() {

        DateFormat dateFormat = new SimpleDateFormat("dd,MM,yyyy");
        Date date = new Date();

        String fileName = (dateFormat.format(date) + ".txt");
        File file = new File(fileName);

        try {
            fileOutputStream = new FileOutputStream((file), true);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        TeeOutputStream myOut = new TeeOutputStream(System.out, fileOutputStream);

        PrintStream printStream = new PrintStream(myOut, true);

        System.setOut(printStream);
    }
}