package rs.devcenter.battlearena.logger;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * HighScore is class which is used to find the "best" Hero from file loggs on that day.
 */
public class HighScore {

    /**
     * this method loops  through file and finds the Hero who won for least number of rounds.
     */
    public void gettingHighScore() throws IOException {
        String winner = "";
        int numOfRounds = 1110;
        int tempNumOfRounds = 0;
        String tempChamp = "";
        Charset charset = null;
        DateFormat dateFormat = new SimpleDateFormat("dd,MM,yyyy");
        Date date = new Date();

        String fileName = (dateFormat.format(date) + ".txt");
        File file = new File(fileName);

        for (String line : FileUtils.readLines(file, charset)) {
            if (line.contains("Winner is")) {
                String tempStr = line.toString();
                String[] parts = tempStr.split(": ");
                tempChamp = parts[1].toString();
            }

            if (line.contains("Number of rounds was:")) {
                System.out.println("\n");
                String tempStr = line.toString();
                String[] parts = tempStr.split(": ");
                String part1 = parts[1];
                tempNumOfRounds = Integer.parseInt(part1);

                if (numOfRounds > tempNumOfRounds) {
                    numOfRounds = tempNumOfRounds;
                    winner = tempChamp;
                }
            }
        }
        System.out.println(winner + " has a High Score with " + numOfRounds + " rounds!");
    }
}