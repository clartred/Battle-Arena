package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.hero.Hero;

public class CurseOfManari extends Spell {

    public CurseOfManari(Hero h1) {
        this.setH1(h1);
        this.setName("Curse of Man'ari");
        this.setManaCost(30+(10*h1.getCurrentLv()));
        this.setDamage(20+h1.getAP());
    }

    /**
     * This method will cause damage to the enemy
     * @param hero is a Hero that Dot will be casted upon!
     */
    @Override
    public void spell(Hero hero) {
        if (getH1().getCurrentMana() > getManaCost()) {
            getH1().setCurrentMana(getH1().getCurrentMana() - getManaCost());
            hero.setCurrentHP(hero.getCurrentHP() - getDamage());
            System.out.println(getH1().getName() + " casted Curse of Man'ari for "+  getDamage() + " damage");
        } else {
            System.out.println(getH1().getName() + " doesn't have enough mana");
            hero.setCurrentHP(hero.getCurrentHP() - (getH1().getMinAD()));
        }
    }
}