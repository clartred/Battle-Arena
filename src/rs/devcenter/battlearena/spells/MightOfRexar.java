package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.hero.Hero;

/**
 * This class is a container for a spell method which will be used for combat between Heroes.
 */
public class MightOfRexar extends Spell {

    public MightOfRexar(Hero h1) {
        this.setH1(h1);
        this.setName("Might of Rexar");
        this.setManaCost(20+(15*h1.getCurrentLv()));
        this.setDamage(25+h1.getAP());
    }

    /**
     * Might of Rexar attack is a high-damage attack with low energy cost.
     * If Hero doesn't have enough mana, he will attack with his weapon!
     */
    @Override
    public void spell(Hero enemyHero) {
        if (getH1().getCurrentMana() > getManaCost()) {
            getH1().setCurrentMana(getH1().getCurrentMana() - getManaCost());
            enemyHero.setCurrentHP(enemyHero.getCurrentHP() - getDamage());
            System.out.println(getH1().getName() + " casted Might of Rexar for " +  getDamage() + " damage!");
        } else {
            System.out.println(getH1().getName() + " does't have enough mana ");
            enemyHero.setCurrentHP(enemyHero.getCurrentHP() - (getH1().getMinAD()));
        }
    }
}