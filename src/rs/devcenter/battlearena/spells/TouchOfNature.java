package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.hero.Hero;

public class TouchOfNature extends Spell {
    public TouchOfNature(Hero h1) {
        this.setH1(h1);
        this.setName("Touch of Nature");
        this.setManaCost(25+(10*h1.getCurrentLv()));
        this.setDamage(25+h1.getAP());
    }

    /**
     * Penance is a healing spell.
     * If Hero doesn't have enough mana, he will attack with his weapon!
     */
    @Override
    public void spell(Hero enemyHero) {
        if (getH1().getCurrentMana() > getManaCost()) {
            getH1().setCurrentMana(getH1().getCurrentMana() - getManaCost());
            getH1().setCurrentHP(getH1().getCurrentHP() + getDamage());
            System.out.println(getH1().getName() + " healed himself for " + getDamage());
        } else {
            System.out.println(getH1().getName() + " doesn't have enough mana.");
        }
        System.out.println("STORM, EARTH AND FIRE HEAR MY CALL!");
    }
}