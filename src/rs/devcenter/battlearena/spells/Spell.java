package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.hero.Hero;

public abstract class Spell {

    private Hero h1, h2;
    private double damage;
    private double manaCost;
    private String name;

    public abstract void spell(Hero h);

    public Hero getH1() {
        return h1;
    }

    public void setH1(Hero h1) {
        this.h1 = h1;
    }

    public Hero getH2() {
        return h2;
    }

    public void setH2(Hero h2) {
        this.h2 = h2;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getDamage() {
        return damage;
    }

    public void setDamage(double damage) {
        this.damage = damage;
    }

    public double getManaCost() {
        return manaCost;
    }

    public void setManaCost(double manaCost) {
        this.manaCost = manaCost;
    }
}