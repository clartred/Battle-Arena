package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.hero.Hero;

public class ArcaneBlast extends Spell {
    public ArcaneBlast(Hero h1) {
        this.setH1(h1);
        this.setName("Arcane Blast");
        this.setManaCost(30+(15*h1.getCurrentLv()));
        this.setDamage(30+h1.getAP());
    }

    /**
     * Arcane Blast is a high damage spell with high Mana cost.
     * If Hero doesn't have enough mana, he will attack with his weapon!
     */
    public void spell(Hero enemyHero) {
        if (getH1().getCurrentMana() > getManaCost()) {
            getH1().setCurrentMana(getH1().getCurrentMana() - getManaCost());
            enemyHero.setCurrentHP(enemyHero.getCurrentHP() - getDamage());
            System.out.println(getH1().getName() + " casted Arcane Blast for "+  getDamage() + " damage");
        } else {
            System.out.println(getH1().getName() + " does't have enough mana ");
            enemyHero.setCurrentHP(enemyHero.getCurrentHP() - (getH1().getMinAD()));
        }
    }
}