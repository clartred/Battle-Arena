package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.hero.Hero;

public class WhispersOfNzoth extends Spell{

    public WhispersOfNzoth(Hero h1) {
        this.setH1(h1);
        this.setName("Whisper of N'zoth");
        this.setManaCost(30+(10*h1.getCurrentLv()));
        this.setDamage(30+h1.getAP());
    }

    /**
     * Whispers of N'zoth is a medium cost, medium damage spell.
     * If Hero doesn't have enough mana, he will attack with his weapon!
     */
    @Override
    public void spell(Hero enemyHero) {
        if (getH1().getCurrentMana() > getManaCost()) {
            getH1().setCurrentMana(getH1().getCurrentMana() - getManaCost());
            enemyHero.setCurrentHP(enemyHero.getCurrentHP() - getDamage());
            System.out.println(getH1().getName() + " casted Whispers of N'zoth for " +  getDamage() + " damage!");
        } else {
            System.out.println(getH1().getName() + " doesn't have enough mana");
            enemyHero.setCurrentHP(enemyHero.getCurrentHP() - (getH1().getMinAD()));
        }
    }
}