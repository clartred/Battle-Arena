package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.hero.Hero;


public class MightOfYoggSaron extends Spell{

    public MightOfYoggSaron(Hero h1) {
        this.setH1(h1);
        this.setName("Might of Yogg'Saron");
        this.setManaCost(25+(15*h1.getCurrentLv()));
        this.setDamage(35+h1.getAP());
    }

    /**
     * Might of Yogg'Saron is a medium cost, medium damage spell.
     * If Hero doesn't have enough mana, he will attack with his weapon!
     */
    @Override
    public void spell(Hero enemyHero) {
        if (getH1().getCurrentMana() > getManaCost()) {
            getH1().setCurrentMana(getH1().getCurrentMana() - getManaCost());
            enemyHero.setCurrentHP(enemyHero.getCurrentHP() - getDamage());
            System.out.println(getH1().getName() + " casted Might of Yogg'Saron for " +  getDamage() + " damage!");
        } else {
            System.out.println(getH1().getName() + " doesn't have enough mana");
            System.out.print("The end of days is finally upon you and all who ");
            System.out.println("inhabits this miserable little seedling.");
            enemyHero.setCurrentHP(enemyHero.getCurrentHP() - (getH1().getMinAD()));
        }
    }
}