package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.hero.Hero;

public class DrainLife extends Spell {

    public DrainLife(Hero h1) {
        this.setH1(h1);
        this.setName("Drain life");
        this.setManaCost(30 + (15 * h1.getCurrentLv()));
        this.setDamage(20 + h1.getAP());
    }

    /**
     * Drain life  is healing/damage spell.
     * If Hero doesn't have enough mana, he will attack with his weapon!
     */
    @Override
    public void spell(Hero enemyHero) {
        if (getH1().getCurrentMana() > getManaCost()) {
            getH1().setCurrentMana(getH1().getCurrentMana() - getManaCost());
            if ((getH1().getCurrentHP() + 40) > getH1().getMaxHP()) {
                getH1().setCurrentHP(getH1().getMaxHP());
            } else getH1().setCurrentHP(getH1().getCurrentHP() + (getDamage() + 25));
            enemyHero.setCurrentHP(enemyHero.getCurrentHP() - getDamage());
            System.out.println(getH1().getName() + " casted Drain life and damaged " + enemyHero.getName() + " for " + getDamage() + " and healed himself for " + getDamage() + " health");

        } else {
            System.out.println(getH1().getName() + " doesn't have enough mana");
            enemyHero.setCurrentHP(enemyHero.getCurrentHP() - (getH1().getMinAD()));
        }
    }
}
