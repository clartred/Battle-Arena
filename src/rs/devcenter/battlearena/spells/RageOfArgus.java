package rs.devcenter.battlearena.spells;

import rs.devcenter.battlearena.hero.Hero;

public class RageOfArgus extends Spell {
    public RageOfArgus(Hero h1) {
        this.setH1(h1);
        this.setName("Rage of Argus");
        this.setManaCost(20+(15*h1.getCurrentLv()));
        this.setDamage(30+h1.getAP());
    }

    /**
     * Rage of Argus is a medium cost, medium damage spell.
     * If Hero doesn't have enough mana, he will attack with his weapon!
     */
    @Override
    public void spell(Hero enemyHero) {
        if (getH1().getCurrentMana() > getManaCost()) {
            getH1().setCurrentMana(getH1().getCurrentMana() - getManaCost());
            enemyHero.setCurrentHP(enemyHero.getCurrentHP() - getDamage());
            System.out.println(getH1().getName() + " casted Rage of Argus for " +  getDamage() + " damage!");
        } else {
            System.out.println(getH1().getName() + " doesn't have enough mana");
            enemyHero.setCurrentHP(enemyHero.getCurrentHP() - (getH1().getMinAD()));
        }
    }
}