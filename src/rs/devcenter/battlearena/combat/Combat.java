package rs.devcenter.battlearena.combat;

import rs.devcenter.battlearena.effectovertime.EffectOverTime;
import rs.devcenter.battlearena.hero.*;
import rs.devcenter.battlearena.spells.Spell;

import java.util.Random;
import java.util.Scanner;

/**
 * This is a abstract class Combat which will be inherited by all other types of Combat.
 * It is used to simulate different kinds of combat between User and AI.
 *
 * @author Nemanja Rajkovic
 */
public abstract class Combat {
    private Random r;
    private Scanner sc;
    private Hero hero1, hero2;
    private int roundCounter;

    public abstract void userCombat();

    public abstract void AICombat();

    public Hero getHero1() {
        return hero1;
    }

    public void setHero1(Hero hero1) {
        this.hero1 = hero1;
    }

    public Hero getHero2() {
        return hero2;
    }

    public void setHero2(Hero hero2) {
        this.hero2 = hero2;
    }

    public int getRoundCounter() {
        return roundCounter;
    }

    public void setRoundCounter(int roundCounter) {
        this.roundCounter = roundCounter;
    }

    public Random getR() {
        return r;
    }

    public void setR(Random r) {
        this.r = r;
    }

    public Scanner getSc() {
        return sc;
    }

    public void setSc(Scanner sc) {
        this.sc = sc;
    }
}