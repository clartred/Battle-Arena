package rs.devcenter.battlearena.arena;

import rs.devcenter.battlearena.battlemode.ChooseAbilitiesMode;
import rs.devcenter.battlearena.battlemode.RegularMode;
import rs.devcenter.battlearena.hero.*;

import java.sql.SQLException;
import java.util.Scanner;

/**
 * Basic Arena is a class which inherits abstract class Arena.
 * This arena will have no handicap or advantages.
 */
public class BasicArena extends Arena {
    private Scanner scanner = new Scanner(System.in);

    public BasicArena(Hero hero1, Hero hero2) {
        this.setHero1(hero1);
        this.setHero2(hero2);
    }

    /**
     * This method lets User picks battle Mode and starts battle.
     * And it prints the winner of the combat.
     */
    public void startCombat() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        System.out.print("Choose Battle Mode : 1. User v AI , 2. Choose Ability Mode ");
        int i = scanner.nextInt();
        if (i == 1) {
            RegularMode regularMode = new RegularMode(getHero1(), getHero2());
            regularMode.startCombat();
        } else if (i == 2) {
            ChooseAbilitiesMode chooseAbilitiesMode = new ChooseAbilitiesMode(getHero1(), getHero2());
            chooseAbilitiesMode.startCombat();
        }
    }
}