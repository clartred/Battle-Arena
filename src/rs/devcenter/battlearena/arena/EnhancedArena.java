package rs.devcenter.battlearena.arena;

import rs.devcenter.battlearena.battlemode.ChooseAbilitiesMode;
import rs.devcenter.battlearena.battlemode.RegularMode;
import rs.devcenter.battlearena.buffs.ADBuff;
import rs.devcenter.battlearena.buffs.APBuff;
import rs.devcenter.battlearena.buffs.EnergyBuff;
import rs.devcenter.battlearena.buffs.HealthBuff;
import rs.devcenter.battlearena.hero.Hero;

import java.sql.SQLException;
import java.util.Scanner;

/**
 * This arena will buff up heroes inside automaticly.
 *
 * @author Nemanja Rajkovic
 * @version 1.0
 * @since 21.10.2017
 */
public class EnhancedArena extends Arena {

    private Scanner scanner = new Scanner(System.in);

    public EnhancedArena(Hero hero1, Hero hero2) {
        this.setHero1(hero1);
        this.setHero2(hero2);
        ADBuff adBuff = new ADBuff(getHero1(), getHero2());
        APBuff apBuff = new APBuff(getHero1(), getHero2());
        EnergyBuff energyBuff = new EnergyBuff(getHero1(), getHero2());
        HealthBuff healthBuff = new HealthBuff(getHero1(), getHero2());
        adBuff.buff(50);
        apBuff.buff(50);
        energyBuff.buff(200);
        healthBuff.buff(200);
    }

    /**
     * This method lets User picks battle Mode and starts battle.
     * And it prints the winner of the combat.
     */
    public void startCombat() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        System.out.print("Choose Battle Mode : 1. User v AI , 2. Choose Ability Mode ");
        int i = scanner.nextInt();
        if (i == 1) {
            RegularMode regularMode = new RegularMode(getHero1(), getHero2());
            regularMode.startCombat();
        } else if (i == 2) {
            ChooseAbilitiesMode chooseAbilitiesMode = new ChooseAbilitiesMode(getHero1(), getHero2());
            chooseAbilitiesMode.startCombat();
        }
    }
}