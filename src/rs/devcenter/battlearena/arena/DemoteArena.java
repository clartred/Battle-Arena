package rs.devcenter.battlearena.arena;

import rs.devcenter.battlearena.battlemode.ChooseAbilitiesMode;
import rs.devcenter.battlearena.battlemode.RegularMode;
import rs.devcenter.battlearena.debuffs.ADDebuff;
import rs.devcenter.battlearena.debuffs.APDebuff;
import rs.devcenter.battlearena.debuffs.EnergyDebuff;
import rs.devcenter.battlearena.debuffs.HealthDebuff;
import rs.devcenter.battlearena.hero.Hero;

import java.sql.SQLException;
import java.util.Scanner;

/**
 * This arena will de-buff up heroes inside automaticly.
 *
 * @author Nemanja Rajkovic
 * @version 1.0
 * @since 18.10.2017
 */
public class DemoteArena extends Arena {

    private Scanner scanner = new Scanner(System.in);

    public DemoteArena(Hero hero1, Hero hero2) {
        this.setHero1(hero1);
        this.setHero2(hero2);
        ADDebuff adDebuff = new ADDebuff(getHero1(), getHero2());
        APDebuff apDebuff = new APDebuff(getHero1(), getHero2());
        EnergyDebuff energyDebuff = new EnergyDebuff(getHero1(), getHero2());
        HealthDebuff healthDebuff = new HealthDebuff(getHero1(), getHero2());
        adDebuff.debuff(30);
        apDebuff.debuff(30);
        energyDebuff.debuff(150);
        healthDebuff.debuff(100);
    }

    /**
     * This method lets User picks battle Mode and starts battle.
     * And it prints the winner of the combat.
     */
    public void startCombat() {
        System.out.print("Choose Battle Mode : 1. User v AI , 2. Choose Ability Mode ");
        int i = scanner.nextInt();
        if (i == 1) {
            RegularMode regularMode = new RegularMode(getHero1(), getHero2());
            regularMode.startCombat();
        } else if (i == 2) {
            ChooseAbilitiesMode chooseAbilitiesMode = new ChooseAbilitiesMode(getHero1(), getHero2());
            chooseAbilitiesMode.startCombat();
        }
    }
}