package rs.devcenter.battlearena.arena;

import rs.devcenter.battlearena.battlemode.BattleMode;
import rs.devcenter.battlearena.hero.*;

import java.sql.SQLException;

/**
 * Arena is abstract class which will be inherited by other Arena classes.
 * Arena is a place where all fights will be held.
 *
 * @author Nemanja Rajkovic
 * @version 1.0
 * @since 17.10.2017
 */
public abstract class Arena {
    private String name;
    private BattleMode battleMode;
    private Hero hero1, hero2;


    public abstract void startCombat() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BattleMode getBattleMode() {
        return battleMode;
    }

    public void setBattleMode(BattleMode battleMode) {
        this.battleMode = battleMode;
    }

    public Hero getHero1() {
        return hero1;
    }

    public void setHero1(Hero hero1) {
        this.hero1 = hero1;
    }

    public Hero getHero2() {
        return hero2;
    }

    public void setHero2(Hero hero2) {
        this.hero2 = hero2;
    }
}
