package rs.devcenter.battlearena.buffs;

import rs.devcenter.battlearena.hero.Hero;

/**
 * This class contains spell that will buff up Health of the hero/heroes.
 */
public class HealthBuff extends Buffs {

    public HealthBuff(Hero hero, Hero hero2) {
        this.setH1(hero);
        this.setH2(hero2);
        this.setBuffName("Health Buff");
        this.setDuration(0);
    }

    public HealthBuff(Hero hero) {
        this.setH1(hero);
        this.setBuffName("Health Buff");
        this.setDuration(0);
        this.setBuffed(false);

    }

    /**
     * This method checks duration of Buff and remove it if needed.
     *
     * @param roundCounter the number of current round.
     * @param hero         whos buff should be checked.
     */
    public void checkDuration(int roundCounter, Hero hero) {
        if (getDuration() > 0) {
            System.out.println(this.getBuffName() + " will be on " + hero.getName() + " for " + (getDuration() - roundCounter) + " rounds");
        }
        if (roundCounter > getDuration() && this.isBuffed() == true) {
            hero.setMaxHP(hero.getMaxHP() - 200);
            hero.setCurrentHP(hero.getCurrentHP() - 200);
            this.setBuffed(false);
        }
    }

    /**
     * This methiod will buff the hero which is forwaded.
     *
     * @param h1 Hero to be buffed.
     */
    public void buff(Hero h1) {
        if (h1.getCurrentMana() > 40) {
            System.out.println(h1.getName() + " buffed his health for 200");
            h1.setMaxHP(h1.getMaxHP() + 200);
            h1.setCurrentHP(h1.getCurrentHP() + 200);
            h1.setCurrentMana(h1.getCurrentMana() - 40);
            this.setBuffed(true);
        }
    }

    /**
     * This method will buff both Heroes for same amount.
     *
     * @param i is how much Heroes stats will be buffed.
     */
    public void buff(int i) {
        System.out.println(getH1().getName() + "'s and " + getH2().getName() + "'s health is buffed for " + i);
        getH1().setMaxHP(getH1().getMaxHP() + i);
        getH1().setCurrentHP(getH1().getCurrentHP() + i);
        getH2().setMaxHP(getH2().getMaxHP() + i);
        getH2().setCurrentHP(getH2().getCurrentHP() + i);
    }
}
