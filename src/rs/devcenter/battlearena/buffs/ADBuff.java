package rs.devcenter.battlearena.buffs;

import rs.devcenter.battlearena.hero.Hero;

/**
 * This class contains spell that will buff up attack damage of the hero/heroes.
 */
public class ADBuff extends Buffs {
    public ADBuff(Hero hero, Hero hero2) {
        this.setH1(hero);
        this.setH2(hero2);
        this.setBuffName("Attack damage Buff");
        this.setDuration(0);
    }

    public ADBuff(Hero hero) {
        this.setH1(hero);
        this.setBuffName("Attack damage Buff");
        this.setDuration(0);
        this.setBuffed(false);
    }

    /**
     * This method checks duration of Buff and remove it if needed.
     *
     * @param roundCounter the number of current round.
     * @param hero         whos buff should be checked.
     */
    public void checkDuration(int roundCounter, Hero hero) {
        if (getDuration() > 0) {
            System.out.println(this.getBuffName() + " will be on " + hero.getName() + " for " + (getDuration() - roundCounter) + " rounds");
        }
        if (roundCounter > getDuration() && this.isBuffed() == true) {
            hero.setMaxAD(hero.getMaxAD() - 50);
            hero.setMinAD(hero.getMinAD() - 50);
        }
    }

    /**
     * This methiod will buff the hero which is forwaded.
     *
     * @param h1 Hero to be buffed.
     */
    public void buff(Hero h1) {
        if (h1.getCurrentMana() > 40) {
            System.out.println(h1.getName() + " buffed his attack damage for 50");
            h1.setMaxAD(h1.getMaxAD() + 50);
            h1.setMinAD(h1.getMinAD() + 50);
            h1.setCurrentMana(h1.getCurrentMana() - 40);
            this.setBuffed(true);
        }
    }

    /**
     * This method will buff both Heroes for same amount.
     *
     * @param i is how much Heroes stats will be buffed.
     */
    public void buff(int i) {
        System.out.println(getH1().getName() + "'s and " + getH2().getName() + "'s attack damage is buffed for " + i);
        getH1().setMaxAD(getH1().getMaxAD() + i);
        getH1().setMinAD(getH1().getMinAD() + i);
        getH2().setMaxAD(getH2().getMaxAD() + i);
        getH2().setMinAD(getH2().getMinAD() + i);
    }
}