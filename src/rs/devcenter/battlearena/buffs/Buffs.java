package rs.devcenter.battlearena.buffs;

import rs.devcenter.battlearena.hero.Hero;

/**
 * This class is a blueprint for all buffs that will be casted on Heroes.
 * @author Nemanja Rajkovic
 * @date 25.10.2017
 * @version 1.0
 */
public abstract class Buffs {

    private Hero h1, h2;
    private boolean isBuffed;
    private String buffName;
    private int duration;

    public abstract void checkDuration(int roundCounter, Hero hero);

    public abstract void buff(Hero h1);

    public abstract void buff(int i);

    public Hero getH1() {
        return h1;
    }

    public void setH1(Hero h1) {
        this.h1 = h1;
    }

    public Hero getH2() {
        return h2;
    }

    public void setH2(Hero h2) {
        this.h2 = h2;
    }

    public String getBuffName() {
        return buffName;
    }

    public void setBuffName(String buffName) {
        this.buffName = buffName;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public boolean isBuffed() {
        return isBuffed;
    }

    public void setBuffed(boolean buffed) {
        isBuffed = buffed;
    }
}