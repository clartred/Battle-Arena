package rs.devcenter.battlearena.buffs;

import rs.devcenter.battlearena.hero.Hero;

/**
 * This class contains spell that will buff up Energy(Mana) of the hero/heroes.
 */
public class EnergyBuff extends Buffs {

    public EnergyBuff(Hero hero, Hero hero2) {
        this.setH1(hero);
        this.setH2(hero2);
        this.setBuffName("Energy Buff");
        this.setDuration(0);
    }

    public EnergyBuff(Hero hero) {
        this.setH1(hero);
        this.setBuffName("Energy Buff");
        this.setDuration(0);
        this.setBuffed(false);
    }

    /**
     * This method checks duration of Buff and remove it if needed.
     *
     * @param roundCounter the number of current round.
     * @param hero         whos buff should be checked.
     */
    public void checkDuration(int roundCounter, Hero hero) {
        if (getDuration() > 0) {
            System.out.println(this.getBuffName() + " will be on " + hero.getName() + " for " + (getDuration() - roundCounter) + " rounds");
        }
        if (roundCounter > getDuration() && this.isBuffed() == true) {
            hero.setMaxMana(hero.getMaxMana() - 300);
            hero.setCurrentMana(hero.getCurrentMana() - 300);
            this.setBuffed(false);
        }
    }

    /**
     * This methiod will buff the hero which is forwaded.
     *
     * @param h1 Hero to be buffed.
     */
    public void buff(Hero h1) {
        if (h1.getCurrentMana() > 40) {
            System.out.println(h1.getName() + " buffed his energy for 300");
            h1.setMaxMana(h1.getMaxMana() + 300);
            h1.setCurrentMana(h1.getCurrentMana() + 300);
            h1.setCurrentMana(h1.getCurrentMana() - 40);
            this.setBuffed(true);
        }
    }

    /**
     * This method will buff both Heroes for same amount.
     *
     * @param i is how much Heroes stats will be buffed.
     */
    public void buff(int i) {
        System.out.println(getH1().getName() + "'s and " + getH2().getName() + "'s energy is buffed for " + i);
        getH1().setMaxMana(getH1().getMaxMana() + 300);
        getH1().setCurrentMana(getH1().getCurrentMana() + 300);
        getH2().setMaxMana(getH2().getMaxMana() + 300);
        getH2().setCurrentMana(getH2().getCurrentMana() + 300);
    }
}
