package rs.devcenter.battlearena.hero;

import rs.devcenter.battlearena.buffs.ADBuff;
import rs.devcenter.battlearena.debuffs.ADDebuff;
import rs.devcenter.battlearena.effectovertime.EffectOverTime;
import rs.devcenter.battlearena.effectovertime.SnakeBite;
import rs.devcenter.battlearena.spells.*;

/**
 * Hunter class inherits class Hero.
 * This class will be instantiated by User and will be used to fight in Arena against other "Heroes".
 *
 * @author Nemanja Rajkovic
 * @version 1.0
 * @since 17.10.2017
 */
public class Hunter extends Hero {
    public Hunter(String name, int currLv, double currXP) {
        this.setName(name);
        this.setCurrXp(currXP);
        this.setCurrentLv(currLv);
        this.setHeroClass("hunter");
        this.setCurrentHP(150 + 100 * getCurrentLv());
        this.setMaxHP(150 + 100 * getCurrentLv());
        this.setCurrentMana(250 + 150 * getCurrentLv());
        this.setMaxMana(350 + 150 * getCurrentLv());
        this.setMinAD(5 + 5 * getCurrentLv());
        this.setMaxAD(10 + 5 * getCurrentLv());
        this.setCritChance(25 + 10 * getCurrentLv());
        this.setAP(25 + (15 * getCurrentLv()+(getMinAD())));
        this.setHeroSpells(new Spell[3]);
        this.setEffectsOverTime(new EffectOverTime[1]);
        ArcaneShot arcaneShot = new ArcaneShot(this);
        MightOfRexar mightOfRexar = new MightOfRexar(this);
        RaptorStrike raptorStrike = new RaptorStrike(this);
        this.setHeroSpells(arcaneShot, 0);
        this.setHeroSpells(mightOfRexar, 1);
        this.setHeroSpells(raptorStrike, 2);
        SnakeBite snakeBite = new SnakeBite(this);
        this.setEffectsOverTime(snakeBite);
        ADBuff adBuff = new ADBuff(this);
        ADDebuff adDebuff = new ADDebuff(this);
        this.setBuff(adBuff);
        this.setDebuff(adDebuff);
    }
}