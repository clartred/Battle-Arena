package rs.devcenter.battlearena.hero;

import rs.devcenter.battlearena.buffs.APBuff;
import rs.devcenter.battlearena.debuffs.APDebuff;
import rs.devcenter.battlearena.effectovertime.EffectOverTime;
import rs.devcenter.battlearena.effectovertime.HealingTotem;
import rs.devcenter.battlearena.spells.ElementalStrike;
import rs.devcenter.battlearena.spells.LavaBurst;
import rs.devcenter.battlearena.spells.Spell;
import rs.devcenter.battlearena.spells.TouchOfNature;

/**
 * Shaman class inherits class Hero.
 * This class will be instantiated by User and will be used to fight in Arena against other "Heroes".
 *
 * @author Nemanja Rajkovic
 * @version 1.0
 * @since 17.10.2017
 */
public class Shaman extends Hero {
    public Shaman(String name, int currLv, double currXP) {
        this.setName(name);
        this.setCurrXp(currXP);
        this.setCurrentLv(currLv);
        this.setHeroClass("shaman");
        this.setCurrentHP(100 + 100 * getCurrentLv());
        this.setMaxHP(100 + 150 * getCurrentLv());
        this.setCurrentMana(300 + 150 * getCurrentLv());
        this.setMaxMana(300 + 150 * getCurrentLv());
        this.setMinAD(5 + 5 * getCurrentLv());
        this.setMaxAD(10 + 5 * getCurrentLv());
        this.setCritChance(10 + 5 * getCurrentLv());
        this.setAP(20 + 20 * getCurrentLv());
        this.setHeroSpells(new Spell[3]);
        this.setEffectsOverTime(new EffectOverTime[1]);
        ElementalStrike elementalStrike = new ElementalStrike(this);
        LavaBurst lavaBurst = new LavaBurst(this);
        TouchOfNature touchOfNature = new TouchOfNature(this);
        this.setHeroSpells(elementalStrike, 0);
        this.setHeroSpells(lavaBurst, 1);
        this.setHeroSpells(touchOfNature, 2);
        HealingTotem healingTotem = new HealingTotem(this);
        this.setEffectsOverTime(healingTotem);
        APDebuff apDebuff = new APDebuff(this);
        APBuff apBuff = new APBuff(this);
        this.setBuff(apBuff);
        this.setDebuff(apDebuff);
    }
}