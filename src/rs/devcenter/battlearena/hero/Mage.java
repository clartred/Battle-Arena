package rs.devcenter.battlearena.hero;

import rs.devcenter.battlearena.buffs.APBuff;
import rs.devcenter.battlearena.debuffs.APDebuff;
import rs.devcenter.battlearena.effectovertime.EffectOverTime;
import rs.devcenter.battlearena.spells.*;
import rs.devcenter.battlearena.effectovertime.FireBomb;

/**
 * Mage class inherits class Hero.
 * This class will be instantiated by User and will be used to fight in Arena against other "Heroes".
 *
 * @author Nemanja Rajkovic
 * @version 1.0
 * @since 17.10.2017
 */
public class Mage extends Hero {
    Spell spells[] = new Spell[3];
    public Mage(String name, int currLv, double currXP) {
        this.setName(name);
        this.setCurrXp(currXP);
        this.setCurrentLv(currLv);
        this.setHeroClass("mage");
        this.setCurrentHP(150 + 100 * getCurrentLv());
        this.setMaxHP(150 + 100 * getCurrentLv());
        this.setCurrentMana(350 + 150 * getCurrentLv());
        this.setMaxMana(350 + 150 * getCurrentLv());
        this.setMinAD(5 + 5 * getCurrentLv());
        this.setMaxAD(10 + 5 * getCurrentLv());
        this.setCritChance(10 + 10 * getCurrentLv());
        this.setAP(25 + (15 * getCurrentLv()));
        this.setHeroSpells(new Spell[3]);
        this.setEffectsOverTime(new EffectOverTime[1]);
        ArcaneBlast arcaneBlast = new ArcaneBlast(this);
        FireBall fireBall = new FireBall(this);
        ImageOfMedvieh imageOfMedvieh = new ImageOfMedvieh(this);
        this.setHeroSpells(arcaneBlast,0);
        this.setHeroSpells(fireBall,1);
        this.setHeroSpells(imageOfMedvieh,2);
        FireBomb fireBomb = new FireBomb(this);
        this.setEffectsOverTime(fireBomb);
        APBuff apBuff = new APBuff(this);
        APDebuff apDebuff = new APDebuff(this);
        this.setBuff(apBuff);
        this.setDebuff(apDebuff);
    }
}