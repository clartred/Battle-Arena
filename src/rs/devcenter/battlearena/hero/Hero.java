package rs.devcenter.battlearena.hero;

import rs.devcenter.battlearena.buffs.Buffs;
import rs.devcenter.battlearena.debuffs.Debuffs;
import rs.devcenter.battlearena.effectovertime.*;
import rs.devcenter.battlearena.spells.*;

/**
 * Hero is an abstract class which will be inherited by other Hero-type classes.
 *
 * @author Nemanja Rajkovic
 * @version 1.0
 * @since 17.10.2017
 */
public abstract class Hero {

    private String name;
    private double currentHP, maxHP;
    private double currentMana, maxMana;
    private double minAD, maxAD;
    private String heroClass;
    private static int maxLevel = 10;
    private int currentLv = 1;
    private double critChance;
    private double currXp; //resets with new lv.
    private double maxXP = 100 + currentLv * 25;
    private double AP;
    private double dotHotCounter;

    private Spell[] heroSpells;
    private EffectOverTime[] effectsOverTime;
    private Buffs buff;
    private Debuffs debuff;

    public Buffs getBuff() {
        return buff;
    }

    public void setBuff(Buffs buff) {
        this.buff = buff;
    }

    public Debuffs getDebuff() {
        return debuff;
    }

    public void setDebuff(Debuffs debuff) {
        this.debuff = debuff;
    }

    public EffectOverTime getEffectsOverTime() {
        return effectsOverTime[0];
    }

    public void setEffectsOverTime(EffectOverTime effectsOverTime) {
        this.effectsOverTime[0] = effectsOverTime;
    }

    public void setEffectsOverTime(EffectOverTime[] effectsOverTime) {
        this.effectsOverTime = effectsOverTime;
    }

    public double getCurrentMana() {
        return currentMana;
    }

    public void setCurrentMana(double currentMana) {
        this.currentMana = currentMana;
    }

    public double getMaxMana() {
        return maxMana;
    }

    public void setMaxMana(double maxMana) {
        this.maxMana = maxMana;
    }

    public double getAP() {
        return AP;
    }

    public void setAP(double AP) {
        this.AP = AP;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getCurrentHP() {
        return currentHP;
    }

    public void setCurrentHP(double currentHP) {
        this.currentHP = currentHP;
    }

    public double getMaxHP() {
        return maxHP;
    }

    public void setMaxHP(double maxHP) {
        this.maxHP = maxHP;
    }

    public static int getMaxLevel() {
        return maxLevel;
    }

    public static void setMaxLevel(int maxLevel) {
        Hero.maxLevel = maxLevel;
    }

    public int getCurrentLv() {
        return currentLv;
    }

    public void setCurrentLv(int currentLv) {
        this.currentLv = currentLv;
    }

    public double getCritChance() {
        return critChance;
    }

    public void setCritChance(double critChance) {
        this.critChance = critChance;
    }

    public double getCurrXp() {
        return currXp;
    }

    public void setCurrXp(double currXp) {
        this.currXp = currXp;
    }

    public double getMaxXP() {
        return maxXP;
    }

    public void setMaxXP(double maxXP) {
        this.maxXP = maxXP;
    }

    public double getMinAD() {
        return minAD;
    }

    public void setMinAD(double minAD) {
        this.minAD = minAD;
    }

    public double getMaxAD() {
        return maxAD;
    }

    public void setMaxAD(double maxAD) {
        this.maxAD = maxAD;
    }

    public double getDotHotCounter() {
        return dotHotCounter;
    }

    public void setDotHotCounter(double dotHotCounter) {
        this.dotHotCounter = dotHotCounter;
    }

    public String getHeroClass() {
        return heroClass;
    }

    public void setHeroClass(String heroClass) {
        this.heroClass = heroClass;
    }

    public void setHeroSpells(Spell heroSpells, int i) {
        this.heroSpells[i] = heroSpells;
    }

    public void setHeroSpells(Spell[] heroSpells) {
        this.heroSpells = heroSpells;
    }

    public Spell getHeroSpells(int i) {
        return heroSpells[i];
    }
}