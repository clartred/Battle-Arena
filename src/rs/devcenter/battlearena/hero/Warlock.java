package rs.devcenter.battlearena.hero;

import rs.devcenter.battlearena.buffs.EnergyBuff;
import rs.devcenter.battlearena.debuffs.EnergyDebuff;
import rs.devcenter.battlearena.effectovertime.Doom;
import rs.devcenter.battlearena.effectovertime.EffectOverTime;
import rs.devcenter.battlearena.spells.CurseOfManari;
import rs.devcenter.battlearena.spells.DrainLife;
import rs.devcenter.battlearena.spells.RageOfArgus;
import rs.devcenter.battlearena.spells.Spell;

/**
 * Warlock class inherits class Hero.
 * This class will be instantiated by User and will be used to fight in Arena against other "Heroes".
 *
 * @author Nemanja Rajkovic
 * @version 1.0
 * @since 17.10.2017
 */
public class Warlock extends Hero {
    public Warlock(String name, int currLv, double currXP) {
        this.setName(name);
        this.setCurrXp(currXP);
        this.setCurrentLv(currLv);
        this.setHeroClass("warlock");
        this.setCurrentHP(200 + 115 * getCurrentLv());
        this.setMaxHP(200 + 150 * getCurrentLv());
        this.setCurrentMana(300 + 250 * getCurrentLv());
        this.setMaxMana(300 + 250 * getCurrentLv());
        this.setMinAD(10 + 5 * getCurrentLv());
        this.setMaxAD(10 + 10 * getCurrentLv());
        this.setCritChance(10 + 5 * getCurrentLv());
        this.setAP(15 + 15 * getCurrentLv());
        this.setHeroSpells(new Spell[3]);
        this.setEffectsOverTime(new EffectOverTime[1]);
        CurseOfManari curseOfManari = new CurseOfManari(this);
        DrainLife drainLife = new DrainLife(this);
        RageOfArgus rageOfArgus = new RageOfArgus(this);
        this.setHeroSpells(drainLife, 0);
        this.setHeroSpells(rageOfArgus, 1);
        this.setHeroSpells(curseOfManari, 2);
        Doom doom = new Doom(this);
        this.setEffectsOverTime(doom);
        EnergyBuff energyBuff = new EnergyBuff(this);
        EnergyDebuff energyDebuff = new EnergyDebuff(this);
        this.setBuff(energyBuff);
        this.setDebuff(energyDebuff);
    }
}