package rs.devcenter.battlearena.hero;

import rs.devcenter.battlearena.buffs.HealthBuff;
import rs.devcenter.battlearena.debuffs.HealthDebuff;
import rs.devcenter.battlearena.effectovertime.BlessingOfNaaru;
import rs.devcenter.battlearena.effectovertime.EffectOverTime;
import rs.devcenter.battlearena.spells.*;

/**
 * Priest class inherits class Hero.
 * This class will be instantiated by User and will be used to fight in Arena against other "Heroes".
 *
 * @author Nemanja Rajkovic
 * @version 1.0
 * @since 17.10.2017
 */
public class Priest extends Hero {
    public Priest(String name, int currLv, double currXP) {
        this.setName(name);
        this.setCurrXp(currXP);
        this.setCurrentLv(currLv);
        this.setHeroClass("priest");
        this.setCurrentHP(150 + 100 * getCurrentLv());
        this.setMaxHP(150 + (100 * getCurrentLv()));
        this.setCurrentMana(300 + 150 * getCurrentLv());
        this.setMaxMana(300 + 150 * getCurrentLv());
        this.setMinAD(5 + 5 * getCurrentLv());
        this.setMaxAD(10 + 5 * getCurrentLv());
        this.setCritChance(10 + 5 * getCurrentLv());
        this.setAP(35 + 15 * getCurrentLv());
        this.setHeroSpells(new Spell[3]);
        this.setEffectsOverTime(new EffectOverTime[1]);
        HolyLight holyLight = new HolyLight(this);
        MightOfYoggSaron mightOfYoggSaron = new MightOfYoggSaron(this);
        WhispersOfNzoth whispersOfNzoth = new WhispersOfNzoth(this);
        this.setHeroSpells(mightOfYoggSaron,0);
        this.setHeroSpells(whispersOfNzoth,1);
        this.setHeroSpells(holyLight,2);
        BlessingOfNaaru blessingOfNaaru = new BlessingOfNaaru(this);
        this.setEffectsOverTime(blessingOfNaaru);
        HealthBuff healthBuff = new HealthBuff(this);
        HealthDebuff healthDebuff = new HealthDebuff(this);
        this.setBuff(healthBuff);
        this.setDebuff(healthDebuff);
    }
}