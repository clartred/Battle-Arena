package rs.devcenter.battlearena.effectovertime;

import rs.devcenter.battlearena.hero.Hero;

/**
 * This class will be used for casting a HoT - Healing Totem on a Hero.
 * @author Rajkovic Nemanja
 * @version 1.0
 * @date 25.10.2017
 */
public class HealingTotem extends EffectOverTime {

    public HealingTotem(Hero h1) {
        this.setH1(h1);
        this.setDotHotName("Healing Totem");
        this.setDuration(0);
    }

    /**
     * This method checks round counter and Dot/Hot counter and tickles if needed.
     *
     * @param roundCounter the number of current Round.
     * @param hero Hero on whom Dot/Hot is.
     */
    public void checkDuration(int roundCounter, Hero hero) {
        if (roundCounter < this.getDuration()) {
            spell(getH1());
        }
    }

    /**
     * This method will heal our Hero.
     *
     * @param hero is a that HOT will be casted on.
     */
    @Override
    public void spell(Hero hero) {
        if(this.getDuration() == 0){
            System.out.println(getH1().getName() + " casted " + this.getDotHotName());
        }
        if (getH1().getCurrentMana() > 20) {
            getH1().setCurrentMana(getH1().getCurrentMana() - 20);
            getH1().setCurrentHP(getH1().getCurrentHP() + 30);
            System.out.println(getH1().getName() + " healed himself for  30 hp."  );
        } else {
            System.out.println(getH1().getName() + " doesn't have enough mana");
        }
    }
}
