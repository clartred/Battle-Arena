package rs.devcenter.battlearena.effectovertime;

import rs.devcenter.battlearena.hero.Hero;

/**
 * This class is blueprint for all Dots and Hots(Damage over time, healing over time) that will be
 * casted on Hero.
 *
 * @author Rajkovic Nemanja
 * @version 1.0
 * @date 25.10.2017
 */
public abstract class EffectOverTime {

    private Hero h1, h2;
    private int duration;
    private String dotHotName;

    public abstract void checkDuration(int roundCounter, Hero hero);

    public abstract void spell(Hero h1);

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public Hero getH1() {
        return h1;
    }

    public void setH1(Hero h1) {
        this.h1 = h1;
    }

    public Hero getH2() {
        return h2;
    }

    public void setH2(Hero h2) {
        this.h2 = h2;
    }

    public String getDotHotName() {
        return dotHotName;
    }

    public void setDotHotName(String dotHotName) {
        this.dotHotName = dotHotName;
    }
}