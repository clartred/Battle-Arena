package rs.devcenter.battlearena;

import rs.devcenter.battlearena.arena.BasicArena;
import rs.devcenter.battlearena.arena.DemoteArena;
import rs.devcenter.battlearena.arena.EnhancedArena;
import rs.devcenter.battlearena.effectovertime.*;
import rs.devcenter.battlearena.hero.*;
import rs.devcenter.battlearena.logger.BattleLogger;
import rs.devcenter.battlearena.spells.*;

import java.sql.SQLException;
import java.util.Random;
import java.util.Scanner;

/**
 * This class lets User choose which spells he wants to use, which champion
 * he wants to play and what arena he wants to play in.
 *
 * @author Nemanja Rajkovic
 * @version 1.0
 * @date 30.10.2017
 */
public class StartingGame {

    private DBConnection dbConnection = new DBConnection();
    private Hero hero1, hero2;
    private Scanner scanner = new Scanner(System.in);

    /**
     * This method lets Hero choose which Hero he wants to play.
     *
     * @param i number of a hero User will play.
     * @param j number of a hero AI will play.
     */
    public void chooseHero(int i, int j) throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        switch (i) {
            case 1: {
                Priest priest = dbConnection.retrievePriest();
                hero1 = priest;
            }
            break;
            case 2: {
                Mage mage = dbConnection.retrieveMage();
                hero1 = mage;
            }
            break;
            case 3: {
                Warlock warlock = dbConnection.retrieveWarlock();
                hero1 = warlock;
            }
            break;
            case 4: {
                Hunter hunter = dbConnection.retrieveHunter();
                hero1 = hunter;
            }
            break;
            case 5: {
                Shaman shaman = dbConnection.retrieveShaman();
                hero1 = shaman;
            }
        }

        switch (j) {
            case 1: {
                Priest priest = new Priest("AI Priest", hero1.getCurrentLv(), 1);
                hero2 = priest;
            }
            break;
            case 2: {
                Mage mage = new Mage("AI Mage", hero1.getCurrentLv(), 1);
                hero2 = mage;
            }
            break;
            case 3: {
                Warlock warlock = new Warlock("AI Warlock", hero1.getCurrentLv(), 1);
                hero2 = warlock;
            }
            break;
            case 4: {
                Hunter hunter = new Hunter("AI Hunter", hero1.getCurrentLv(), 1);
                hero2 = hunter;
            }
            break;
            case 5: {
                Shaman shaman = new Shaman("AI Shaman", hero1.getCurrentLv(), 1);
                hero2 = shaman;
                break;
            }
        }
    }

    /**
     * This methods lets User choose which Arena he wants to play.
     */
    public void chooseArena() throws ClassNotFoundException, SQLException, InstantiationException, IllegalAccessException {
        System.out.println("Choose Arena you want: 1 for Basic Arena, 2 for Demote Arena, 3 for Enhanced Arena");
        int j = scanner.nextInt();
        switch (j) {

            case 1:
                BasicArena basicArena = new BasicArena(hero1, hero2);
                basicArena.startCombat();
                break;
            case 2:
                DemoteArena demoteArena = new DemoteArena(hero1, hero2);
                demoteArena.startCombat();
                break;
            case 3:
                EnhancedArena enhancedArena = new EnhancedArena(hero1, hero2);
                enhancedArena.startCombat();
                break;
        }
    }

    /**
     * This method is starting Game. It forwards User to choose Hero, Arena, Spells and Dots/Hots.
     */
    public void startGame() {
        dbConnection.checkIfDBExists();
        BattleLogger battleLogger = new BattleLogger();
        battleLogger.logging();
        System.out.println("Please choose a Class you want to play \n Press: 1 for Priest, 2 for Mage, 3 for Warlock, 4 for Hunter, 5 for Shaman");
        int i = scanner.nextInt();
        Random random = new Random();
        int j = random.nextInt(5) + 1;
        try {
            this.chooseHero(i, j); // HERE IT IS
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
        try {
            this.chooseArena(); // HERE IT IS!
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (InstantiationException e) {
            e.printStackTrace();
        } catch (IllegalAccessException e) {
            e.printStackTrace();
        }
    }
}