package rs.devcenter.battlearena.debuffs;

import rs.devcenter.battlearena.hero.Hero;

/**
 * This class contains spell that will de-buff up health of the hero/heroes.
 */
public class HealthDebuff extends Debuffs {

    public HealthDebuff(Hero hero, Hero hero2) {
        this.setH1(hero);
        this.setH2(hero2);
        this.setDebuffName("Health Debuff");
        this.setDuration(0);
    }

    public HealthDebuff(Hero hero) {
        this.setH1(hero);
        this.setDebuffName("Health Debuff");
        this.setDuration(0);
    }

    /**
     * This method checks round counter and buff timer and remove debuff if it is needed.
     *
     * @param roundCounter the number of current Round.
     * @param hero         Hero on whom debuff is.
     */
    public void checkDuration(int roundCounter, Hero hero) {
        if (getDuration() > 0) {
            System.out.println(this.getDebuffName() + " will be on " + hero.getName() + " for " + (getDuration() - roundCounter) + "  rounds");
        }
        if (roundCounter > getDuration() && this.isDebuffed() == true) {
            hero.setMaxHP(hero.getMaxHP() + 50);
            hero.setCurrentHP(((hero.getCurrentHP() + 50)));
            this.setDebuffed(false);
        }
    }

    /**
     * This methiod will de-buff the hero which is forwaded.
     *
     * @param h1 is the Hero who casts de-buff.
     * @param h2 is the Hero that de-buff will be casted on.
     */
    public void debuff(Hero h1, Hero h2) {
        if (h1.getCurrentMana() > 40) {
            System.out.println(h1.getName() + " debuffed " + h2.getName() + "'s health by 50");
            h2.setMaxHP(h2.getMaxHP() - 50);
            h2.setCurrentHP(((h2.getCurrentHP() - 50) < 0) ? 1 : (h2.getCurrentHP() - 50));
            h1.setCurrentMana(h1.getCurrentMana() - 40);
            this.setDebuffed(true);
        } else {
            System.out.println(h1.getName() + " doesn't have enough mana");
        }
    }

    /**
     * This method will buff both Heroes for same amount.
     *
     * @param i is how much Heroes stats will be de-buffed.
     */
    public void debuff(int i) {
        System.out.println(getH1().getName() + "'s and " + getH2().getName() + "'s attack damage is reduced by " + i);
        getH1().setMaxHP(getH1().getMaxHP() - i);
        getH1().setCurrentHP(((getH1().getCurrentHP() - i) < 0) ? 1 : (getH1().getCurrentHP() - i));
        getH2().setMaxHP(getH2().getMaxHP() - i);
        getH2().setCurrentHP(((getH2().getCurrentHP() - i) < 0) ? 1 : (getH2().getCurrentHP() - i));
    }
}