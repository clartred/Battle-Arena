package rs.devcenter.battlearena.debuffs;

import rs.devcenter.battlearena.hero.Hero;

/**
 * This class is a blueprint for all de-buffs that will be casted on Heroes.
 *
 * @author Nemanja Rajkovic
 * @version 1.0
 * @date 25.10.2017
 */
public abstract class Debuffs {

    private boolean isDebuffed;
    private Hero h1, h2;
    private int duration;
    private String debuffName;

    public abstract void checkDuration(int roundCounter, Hero hero);

    public abstract void debuff(Hero h1, Hero h2);

    public abstract void debuff(int i);

    public Hero getH1() {
        return h1;
    }

    public void setH1(Hero h1) {
        this.h1 = h1;
    }

    public Hero getH2() {
        return h2;
    }

    public void setH2(Hero h2) {
        this.h2 = h2;
    }

    public int getDuration() {
        return duration;
    }

    public void setDuration(int duration) {
        this.duration = duration;
    }

    public String getDebuffName() {
        return debuffName;
    }

    public void setDebuffName(String debuffName) {
        this.debuffName = debuffName;
    }

    public boolean isDebuffed() {
        return isDebuffed;
    }

    public void setDebuffed(boolean debuffed) {
        isDebuffed = debuffed;
    }
}