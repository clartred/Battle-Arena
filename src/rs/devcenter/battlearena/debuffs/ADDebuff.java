package rs.devcenter.battlearena.debuffs;

import rs.devcenter.battlearena.hero.Hero;

/**
 * This class contains spell that will de-buff up attack damage of the hero/heroes.
 */
public class ADDebuff extends Debuffs {

    public ADDebuff(Hero hero, Hero hero2) {
        this.setH1(hero);
        this.setH2(hero2);
        this.setDebuffName("Attack Damage Debuff");
        this.setDuration(0);
    }

    public ADDebuff(Hero hero) {
        this.setH1(hero);
        this.setDebuffName("Attack Damage Debuff");
        this.setDuration(0);
        this.setDebuffed(false);
    }

    /**
     * This method checks round counter and buff timer and remove debuff if it is needed.
     *
     * @param roundCounter the number of current Round.
     * @param hero         Hero on whom debuff is.
     */
    public void checkDuration(int roundCounter, Hero hero) {
        if (getDuration() > 0 ) {
            System.out.println(this.getDebuffName() + " will be on " + hero.getName() + " for "+ (getDuration() - roundCounter) + "  rounds");
        }
        if (roundCounter > getDuration() && this.isDebuffed() == true) {
            hero.setMinAD(hero.getMinAD() + 30);
            hero.setMaxAD(hero.getMaxAD() + 30);
            this.setDebuffed(false);
        }
    }

    /**
     * This method will de-buff the hero which is forwaded.
     *
     * @param h1 is the Hero who casts de-buff.
     * @param h2 is the Hero that de-buff will be casted on.
     */
    public void debuff(Hero h1, Hero h2) {
        if (h1.getCurrentMana() > 40) {
            System.out.println(h1.getName() + " debuffed " + h2.getName() + "'s attack damage by 30");
            h2.setMinAD(h2.getMinAD() - 30);
            h2.setMaxAD(h2.getMaxAD() - 30);
            h1.setCurrentMana(h1.getCurrentMana() - 40);
            this.setDebuffed(true);
        } else {
            System.out.println(h1.getName() + " doesn't have enough mana");
        }
    }

    /**
     * This method will buff both Heroes for same amount.
     *
     * @param i is how much Heroes stats will be de-buffed.
     */
    public void debuff(int i) {
        System.out.println(getH1().getName() + "'s and " + getH2().getName() + "'s attack damage is reduced by " + i);
        getH1().setMaxAD(getH1().getMaxAD() - i);
        getH1().setMinAD(getH1().getMinAD() - i);
        getH2().setMaxAD(getH2().getMaxAD() - i);
        getH2().setMinAD(getH2().getMinAD() - i);
    }
}
